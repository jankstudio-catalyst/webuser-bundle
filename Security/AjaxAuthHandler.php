<?php

namespace Catalyst\WebUserBundle\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AjaxAuthHandler implements AuthenticationSuccessHandlerInterface,AuthenticationFailureHandlerInterface
{
    protected $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onAuthenticationSuccess(Request $req, TokenInterface $token)
    {
        $url = $this->session->get('_security.main.target_path');

        // json response
        $data = [
            'success' => true,
            'url' => $url,
            'error' => '',
        ];
        $resp = new JsonResponse($data);
        return $resp;
    }

    public function onAuthenticationFailure(Request $req, AuthenticationException $e)
    {
        $url = $this->session->get('_security.main.target_path');

        // json response
        $data = [
            'success' => false,
            'url' => $url,
            'error' => $e->getMessage(),
        ];
        $resp = new JsonResponse($data);
        return $resp;
    }
}
