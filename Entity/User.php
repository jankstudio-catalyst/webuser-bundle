<?php

namespace Catalyst\WebUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Serializable;
use DateTime;

use Catalyst\AuthBundle\Entity\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_user")
 */
class User extends BaseUser implements Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=80, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
	protected $first_name;

    /**
     * @ORM\Column(type="string", length=100)
     */
	protected $last_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
	protected $contact_number;

    /**
     * @ORM\Column(type="string", length=97)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $email;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", indexBy="id")
     * @ORM\JoinTable(name="user_user_role")
     */
    protected $roles;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $date_create;

    /**
     * @ORM\Column(type="string")
     */
    protected $timezone;

    public function __construct()
    {
        $this->first_name = '';
        $this->last_name = '';
        $this->date_create = new DateTime();

        // default to UTC
        $this->timezone = 'UTC';
        parent::__construct();
    }

    public function __toString()
    {
        return $this->getDisplayName();
    }

    public function getID()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setFirstName($name)
    {
        $this->first_name = $name;
        return $this;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastName($name)
    {
        $this->last_name = $name;
        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getDisplayName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function setContactNumber($num)
    {
        $this->contact_number = $num;
        return $this;
    }

    public function getContactNumber()
    {
        return $this->contact_number;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setEmail($email = null)
    {
        $this->email = $email;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    public function getTimezone()
    {
        return $this->timezone;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

}
