<?php

namespace Catalyst\WebUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Catalyst\AuthBundle\Entity\Role as BaseRole;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_role")
 */
class Role extends BaseRole
{
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     */
    protected $users;

	public function __construct()
	{
        parent::__construct();
	}
}
