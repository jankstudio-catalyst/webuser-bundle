<?php

namespace Catalyst\WebUserBundle\Listener;

use Catalyst\WebUserBundle\Entity\User;
use Catalyst\WebUserBundle\DBALType\DateTimeType;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class TimezoneListener
{
    protected $security;

    public function __construct(Security $sec)
    {
        $this->security = $sec;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $user = $this->security->getUser();
        if ($user == null)
        {
            // error_log('USER IS NULL');
            return;
        }

        if (!$user instanceof User)
        {
            // error_log('user is not a user - ' . get_class($user));
            return;
        }

        if (!defined('USER_TIMEZONE'))
            define('USER_TIMEZONE', $user->getTimezone());
    }
}
