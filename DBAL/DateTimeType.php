<?php

namespace Catalyst\WebUserBundle\DBAL;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateTimeType as Base;

use DateTime;
use DateTimeZone;

class DateTimeType extends Base
{
    // sets the datetime to UTC before saving to database
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateTimeInterface)
        {
            $value->setTimeZone(new DateTimeZone('UTC'));
        }

        return parent::convertToDatabaseValue($value, $platform);
    }

    // 
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null)
        {
            return $value;
        }

        // check if $value is already DateTime
        if ($value instanceof DateTime)
        {
            // assume it was fetched as UTC
            // shift to our timezone
            if (defined('USER_TIMEZONE'))
                $value->setTimeZone(new DateTimeZone(USER_TIMEZONE));

            return $value;
        }

        $tz_utc = new DateTimeZone('UTC');

        // convert string to DateTime
        $val = DateTime::createFromFormat($platform->getDateTimeFormatString(), $value, $tz_utc);

        // conversion fail
        if (!$val)
        {
            // last ditch effort to convert
            $val = date_create($value, $tz_utc);
        }

        // really can't convert
        if (!$val) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), $platform->getDateTimeFormatString());
        }

        // check if we need to set to user timezone
        if (defined('USER_TIMEZONE'))
        {
            $val->setTimeZone(new DateTimeZone(USER_TIMEZONE));

            return $val;
        }

        return $val;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return false;
    }
}
